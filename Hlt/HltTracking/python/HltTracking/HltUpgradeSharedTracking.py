#!/usr/bin/env python
# =============================================================================
## @file HltTracking/HltSharedTracking.py
#  was HltReco.py
#  Collection of predefined algorithms to perform reconstruction
#  @author Sebastian Neubert sebastian.neubert@cern.ch, Gerhard Raven Gerhard.Raven@nikhef.nl
# =============================================================================
#

__all__ = ( 'VeloPix'
          , 'HltHPTTracking'
          , 'VPUTTracking'
          )


############################################################################################
# Option to decide which pattern to use
############################################################################################
#############################################################################################
# Import Configurables
#############################################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import PVOfflineTool
from HltLine.HltLine import bindMembers
from Configurables import PatSeeding, PatSeedingTool

#############################################################################################
# Configure pattern recognition algorithms
#############################################################################################
from HltUpgradeTrackNames import Hlt1TrackLoc, HltSharedTrackLoc, Hlt2TrackLoc
from GaudiKernel.SystemOfUnits import mm
from Configurables import TrackStateInitAlg, TrackStateInitTool
from Configurables import ToolSvc, TrackMasterExtrapolator


#### Velo Tracking

# the full Velo reconstruction
def recoVeloPix(OutputTracksName=HltSharedTrackLoc["VeloPix"]):
    from Configurables import PrPixelTracking
    recoVeloPix = PrPixelTracking('PrVeloHlt', OutputTracksName = OutputTracksName)
    # TODO: Move this to HltRecoConf
    recoVeloPix.StatPrint = True
    recoVeloPix.VetoObjects = [ OutputTracksName ]

    # clusters are decoded on demand, to store them in the TES for the track fit,
    # the PrPixelStoreClusters algorithm is needed.
    from Configurables import PrPixelStoreClusters
    return [recoVeloPix, PrPixelStoreClusters()]

#### filter the Velo output
from Configurables import TrackListRefiner, HltRecoConf
filterVeloPix = TrackListRefiner('VeloPixFilter',
                                 inputLocation = HltSharedTrackLoc["VeloPix"],
                                 outputLocation = HltSharedTrackLoc["VeloPixSelection"])
from Configurables import  LoKi__Hybrid__TrackSelector as LoKiTrackSelector
filterVeloPix.addTool(LoKiTrackSelector,name="VPSelector")
filterVeloPix.Selector = LoKiTrackSelector(name="VPSelector")
veloPixSelector = filterVeloPix.Selector
veloPixSelector.Code = HltRecoConf().getProp("VeloSelectionCut")
veloPixSelector.StatPrint = True

#### VeloUT Tracking
from Configurables import PrVeloUT
recoVeloUT = PrVeloUT('PrVeloUTHlt',
                      InputTracksName = HltSharedTrackLoc["VeloPixSelection"],
                      OutputTracksName = HltSharedTrackLoc["VeloUTHPT"])
recoVeloUT.VetoObjects = [ recoVeloUT.OutputTracksName ]

#### Forward Tracking
from Configurables import PrForwardTracking, PrForwardTool
from Configurables import HltRecoConf
from HltRecoConf import CommonForwardOptions
recoForwardHPT = PrForwardTracking( 'Hlt1UpgradeForwardHPT'
                                  , InputName  = recoVeloUT.OutputTracksName
                                  , OutputName = HltSharedTrackLoc["ForwardHPT"])
recoForwardHPT.VetoObjects = [ recoForwardHPT.OutputName ]

# apply modifications on top
opts = {"Preselection" : True,
        "UseMomentumEstimate" : True,
        "UseWrongSignWindow" : True}
recoForwardHPT.addTool(PrForwardTool(**opts), name = 'PrForwardTool')

# update configuration with HLT specific tweaks
recoForwardHPT.PrForwardTool.MinPt = HltRecoConf().getProp("Forward_HPT_MinPt")
recoForwardHPT.PrForwardTool.UseMomentumEstimate = True
recoForwardHPT.PrForwardTool.StatPrint = True
recoForwardHPT.StatPrint = True

##### Hlt selections
from Configurables import Hlt__TrackFilter as HltTrackFilter
prepare3DVP = HltTrackFilter( 'Hlt1Prepare3DVeloPix'
                            , InputSelection   = "TES:" + HltSharedTrackLoc["VeloPix"]
                            , RequirePositiveInputs = False
                            , Code = [ '~TrBACKWARD' ]
                            , OutputSelection     = "Velo" )

#TODO: Move this to TrackFitter package?
def ConfigureFitter( fitter ):
    from TrackFitter.ConfiguredFitters import ConfiguredHltFitter, ConfiguredMasterFitter
    # configure the fitter
    if not HltRecoConf().getProp("MoreOfflineLikeFit"):
        ConfiguredHltFitter( fitter )
        fitter.NumberFitIterations = HltRecoConf().getProp("FitIterationsInHltFit")
    else:
        ConfiguredMasterFitter( fitter , SimplifiedGeometry = False, LiteClusters = True, MSRossiAndGreisen = HltRecoConf().getProp("NewMSinFit"))
    # Ignore Muon hits
    fitter.MeasProvider.IgnoreMuon = True
    return fitter

#############################################################################################
# Define modules for the reconstruction sequence
#############################################################################################
from HltLine.HltDecodeRaw import DecodeUT, DecodeFT

### define exported symbols (i.e. these are externally visible, the rest is NOT)
#This is the part which is shared between Hlt1 and Hlt2 For the VeloPix, the
VeloPix = bindMembers( None, recoVeloPix( OutputTracksName=HltSharedTrackLoc["VeloPix"] ) ).setOutputSelection( HltSharedTrackLoc["VeloPix"] )
# We have to remove the decoder from the sequence if disabled otherwise the PV unit complains and does not run.
veloPixAlgs = recoVeloPix( OutputTracksName=HltSharedTrackLoc["VeloPix"] )

# TODO: put selection revive/redo here (ask Sebastian)
# for now always redo:
bm_members =  recoVeloPix() + [ filterVeloPix ]
bm_members += DecodeUT.members() + [recoVeloUT]
bm_members += DecodeFT.members() + [recoForwardHPT]

HltHPTTracking = bindMembers(None, bm_members).setOutputSelection( recoForwardHPT.OutputName )
RevivedForward = bindMembers(None, DecodeFT.members() + HltHPTTracking.members() ).setOutputSelection( recoForwardHPT.OutputName )

#VeloTT tracking
vut_members = recoVeloPix() + [ filterVeloPix ]
vut_members += DecodeUT.members() + [ recoVeloUT ]
VeloUTTracking = bindMembers(None, vut_members).setOutputSelection( recoVeloUT.OutputTracksName )
