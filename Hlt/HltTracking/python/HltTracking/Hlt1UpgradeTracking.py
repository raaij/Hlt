# =============================================================================
# Hlt1 specific tracking algorithms and streamer symbols
# Note that the upfront tracking en'block is defined in HltSharedTracking
# and made aavailable to Hlt1 through TrackCandidates (see end of this file)
# ============================================================================
from Gaudi import Configuration
from Configurables import ( HltTrackFilterGhostProb,
                            HltTrackFit )
from HltLine.HltLine import Hlt1Tool

# =============================================================================
## Configuration functions for convenience.
# =============================================================================
__all__ = ( 'LooseForward',
            'FitTrack',
            'FilterGhostProb',
            'VPCandidates',
            'VeloUTCandidates',
            'TrackCandidates')

from GaudiKernel.SystemOfUnits import MeV
from Configurables import HltRecoConf

def ConfiguredFastKalman( parent = None, name = None ) :
    if name == None: name = 'HltUpgradeTrackFit'
    if parent :
        parent.addTool( HltTrackFit, name )
        parent = getattr( parent, name )
    else :  # make a public instance...
        parent = HltTrackFit('HltUpgradeTrackFit')
    from Configurables import (TrackMasterFitter, TrackInitFit , TrackStateInitTool,
                               TrackMasterExtrapolator)
    from TrackFitter.ConfiguredFitters import ConfiguredHltFitter, ConfiguredMasterFitter
    #TODO: This is a block copy from HltSharedTracking because Fit and Fitter naming inconsistency.
    if not HltRecoConf().getProp("InitFits"):
        # this addTool should not be necessary but unfortunately there is a problem with the toolhandle configuration
        parent.addTool( TrackMasterFitter, "Fit")
        fitter = parent.Fit
    else:
        parent.FitterName = "TrackInitFit/Fit"
        parent.addTool( TrackInitFit, name = "Fit")
        parent.Fit.Init = "TrackStateInitTool/StateInit"
        parent.Fit.addTool(TrackStateInitTool, name="StateInit")
        parent.Fit.StateInit.VeloFitterName = "FastVeloFitLHCbIDs"
        parent.Fit.StateInit.UseFastMomentumEstimate = True
        parent.Fit.addTool(TrackMasterFitter, "Fit")
        fitter = parent.Fit.Fit
    # configure the fitter
    from HltUpgradeSharedTracking import ConfigureFitter
    ConfigureFitter( fitter )
    #if not HltRecoConf().getProp("MoreOfflineLikeFit"):
    #    ConfiguredHltFitter( fitter )
    #    fitter.NumberFitIterations = HltRecoConf().getProp("FitIterationsInHltFit")
    #else:
    #    ConfiguredMasterFitter( fitter , SimplifiedGeometry = True, LiteClusters = True)
    #    fitter.MeasProvider.IgnoreMuon = True


def ConfiguredFilterGhostProb( parent, name ) :
    import Configurables
    from HltLine.HltDecodeRaw import DecodeUT

    options = { "GhostIdTool" : "UpgradeGhostId" }

    idTool = Hlt1Tool(getattr(Configurables, options['GhostIdTool']),
                      UseUTLiteClusters = True,
                      UTLiteClusterLocation = DecodeUT.outputSelection())
                      
    if HltRecoConf().getProp("ApplyGHOSTPROBCutInTBTC") or HltRecoConf().getProp("ApplyGHOSTPROBCut"):
        options.update(
            {"MaxChi2PerDoFCut" : HltRecoConf().getProp("MaxTrCHI2PDOF"),
             "MaxGhostProbCut"   : HltRecoConf().getProp("MaxTrGHOSTPROB") }
            )
                      
    tool =  Hlt1Tool( HltTrackFilterGhostProb
                      , name
                      , tools = [idTool]
                      , **options
                      ).createConfigurable( parent )
    return tool

# =============================================================================
## Symbols for streamer users
# =============================================================================
from Hlt1UpgradeTrackNames import Hlt1Tools
def to_name( key ):
    return Hlt1Tools[key].split('/')[-1]

from Hlt1UpgradeTrackNames import Hlt1CacheLoc, LocToName
def cache_loc(trackLoc):
    return Hlt1CacheLoc[LocToName[trackLoc.split('/')[-1]]]

import HltLine.HltDecodeRaw
from Gaudi.Configuration import ToolSvc
from HltUpgradeSharedTracking import HltHPTTracking

# =============================================================================
## Hlt trackfit upgrade configuration
# =============================================================================
ConfiguredFastKalman( parent = None, name = to_name( "FitTrack" ) )
ConfiguredFilterGhostProb( parent = ToolSvc(), name = to_name( "FilterGhostProb" ) )
## String for users
if HltRecoConf().getProp("ApplyGHOSTPROBCutInHLT1"):
    FitTrack = "FitTrack = TC_UPGRADE_TR ( '', HltTracking.Hlt1UpgradeStreamerConf.FitTrack ) >>  TC_UPGRADE_TR ( '', HltTracking.Hlt1UpgradeStreamerConf.FilterGhostProb ) "
else:
    FitTrack = "FitTrack = TC_UPGRADE_TR ( '', HltTracking.Hlt1UpgradeStreamerConf.FitTrack ) "

# =============================================================================
## IsMuon
# =============================================================================
## Strings for users
IsMuon = "IsMuon = ( execute(decodeMUON) * TC_UPGRADE_TR( '', HltTracking.Hlt1UpgradeStreamerConf.IsMuon ) )"

# ==============================================================================
# The streamer sources making the output locations of the HltHPTTracking
# sequence available in the streamer framework :
# ==============================================================================

# ==============================================================================
# VeloPix candidates
# ==============================================================================
from Configurables import Hlt__Track2Candidate
import HltLine.HltDecodeRaw
from HltUpgradeSharedTracking import VeloPix
from HltLine.HltLine import bindMembers
def VeloPixCandidates( lineName ):
    selection = 'VeloPixCandidates%s' % lineName
    candidates = Hlt__Track2Candidate (
        'VeloPix2Candidates%s' % lineName,
        Code            = "~TrBACKWARD"    , ## skip backward tracks
        InputSelection  = VeloPix.outputSelection(),
        OutputSelection = selection,
        )

    bm = bindMembers ( None , [ VeloPix, candidates ] )
    return "VeloPixCandidates = execute( %s ) * SELECTION( '%s' )" % \
                ( [ m.getFullName() for m in bm.members() ], selection )

# ==============================================================================
# VeloUT candidates
# ==============================================================================
from Configurables import FillHlt1Cache
from Configurables import Hlt__Track2Candidate
import HltLine.HltDecodeRaw
from HltUpgradeSharedTracking import VeloUTTracking
from HltLine.HltLine import bindMembers
def VeloUTCandidatesAlgos( lineName ):
    selection = 'VeloUTCandidates%s' % lineName
    output = VeloUTTracking.outputSelection()
    candidates = Hlt__Track2Candidate (
        'VeloUT2Candidates%s' % lineName,
        Code="~TrBACKWARD",
        InputSelection  = output,
        OutputSelection = selection
        )
    # Don't need this one, so don't run it.
    # cache = FillHlt1Cache("FillCache_" + output.split('/')[-1],
    #                       InputTracks = output,
    #                       Cache  = cache_loc(output),
    #                       FromTypes = ['Velo'],
    #                       ToTypes = ['Upstream', 'Velo'])
    return bindMembers ( None , [ VeloUTTracking, candidates ] )

def VeloUTCandidates( lineName ):
    bm = VeloUTCandidatesAlgos( lineName )
    return "VeloUTCandidates = execute( %s ) * SELECTION( '%s' )" % \
                ( [ m.getFullName() for m in bm.members() ], bm.outputSelection() )

# ==============================================================================
# Track candidates
# ==============================================================================
def TrackCandidatesAlgos( lineName ):
    veloUTAlgos = VeloUTCandidatesAlgos( lineName )

    selection = 'TrackCandidates%s' % lineName
    output = HltHPTTracking.outputSelection()
    tracks = Hlt__Track2Candidate (
        'Track2Candidates%s' % lineName,
        Code="~TrBACKWARD",
        InputSelection  = output,
        OutputSelection = selection,
        )
    fwdCache = FillHlt1Cache("FillCache_" + output.split('/')[-1],
                             ToTracks = output,
                             Cache = cache_loc(output),
                             FromTypes = ['Upstream', 'Velo'],
                             ToTypes = ['Long'])
    return bindMembers ( None , [ HltHPTTracking,
                                  fwdCache, tracks ] )

def TrackCandidates( lineName ):
    bm = TrackCandidatesAlgos( lineName )
    return "TrackCandidates = execute( %s ) * SELECTION( '%s' )" % \
                ( [ m.getFullName() for m in bm.members() ], bm.outputSelection() )
