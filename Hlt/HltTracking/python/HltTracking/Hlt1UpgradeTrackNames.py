HltGlobalEventPrefix            = ""
HltGlobalTrackLocation          = "Track"

def _baseTrackLocation(prefix,tracks) :
    return prefix + "/" + HltGlobalTrackLocation + "/" + tracks

Hlt1TracksPrefix            = HltGlobalEventPrefix + "Hlt1"
Hlt1TrackRoot               = Hlt1TracksPrefix + "/" + HltGlobalTrackLocation + "/"

TrackName = { "VeloPix"           : "VeloPix"           # full VP reconstruction
            , "VeloPixSelection"  : "VeloPixSelection"  # Filtered VP tracks
            , 'VeloUTHPT'         : 'VeloUTHPT'
            , "Forward"           : "UpgradeForward"
            , "ForwardHPT"        : "UpgradeForwardHPT"        # Forward for high pt and p thresholds (HLT1-like)
            , "Match"             : "Match"
            , "Long"              : "Long"
            , "Downstream"        : "Downstream"
            , "Best"              : "Best"
            , "FitTrack"          : "UpgradeFitTrack"
            , "FilterGhostProb"   : "UpgradeFilterGhostProb"
            , "IsMuon"            : "UpgradeIsMuon"
            }

LocToName = {v : k for k, v in TrackName.iteritems()}

Hlt1TrackLoc = {name : _baseTrackLocation(Hlt1TracksPrefix, loc) for name, loc in TrackName.iteritems()}
Hlt1CacheLoc = {name : loc + 'Cache' for name, loc in Hlt1TrackLoc.iteritems()}

Hlt1Tools = { "FitTrack"          : "HltTrackFit/HltUpgradeTrackFit"
            , "IsMuon"            : "IsMuonTool/UpgradeIsMuonTool"
            , "FilterGhostProb"   : "HltTrackFilterGhostProb/HltUpgradeTrackFilterGhostProb"
            }
