# A module to hold the hardcoded names of tracks/protoparticles in the Hlt
# and rules for deriving containers from them
__author__  = "R. Aaij roel.aaij@cern.ch"
########################################################################
# Globals
########################################################################
from HltTracking.Hlt1UpgradeTrackNames import HltGlobalEventPrefix
########################################################################
# These "globals" define that Tracks go into .../Track/... and
# protoparticles into .../ProtoP/...
from HltTracking.Hlt1TrackNames import HltGlobalTrackLocation
HltGlobalProtoPLocation         = "ProtoP"
#
########################################################################
# The rules for generating track and proto particle containers
# These rules apply to HLT2 TODO: add rule for HLT1 usable from Hlt1Units
########################################################################
# For tracks, the format is e.g. Hlt2/Track/Unfitted/Forward
#
# First of all we have the "base" track and protoparticle
# location; this just defines that tracks and protoparticles go into
# some_prefix_you_choose/Track/... and some_refix_you_choose/ProtoP/...
#
from HltTracking.Hlt1UpgradeTrackNames import _baseTrackLocation
#
def _baseProtoPLocation(prefix,protos) :
    return prefix + "/" + HltGlobalProtoPLocation + "/" + protos

########################################################################
# Tracks
########################################################################
# prefixes where to put the tracks (these go into the prefix field of
# _trackLocation and _protosLocation)
#
HltSharedTracksPrefix       = HltGlobalEventPrefix + "Hlt"
Hlt2TracksPrefix            = HltGlobalEventPrefix + "Hlt2"

Hlt2TrackRoot               = Hlt2TracksPrefix + "/" + HltGlobalTrackLocation
HltSharedTrackRoot          = HltSharedTracksPrefix + "/" + HltGlobalTrackLocation

Hlt2TrackEffRoot  =  Hlt2TracksPrefix + "/TrackEff"

#
# names of track types (these go into the tracks field of _trackLocation
# and _protosLocation)
#
from HltTracking.Hlt1UpgradeTrackNames import TrackName
# The prefix says where this track has been produced
Hlt2TrackLoc = { name : _baseTrackLocation(Hlt2TracksPrefix,loc) for name,loc in TrackName.iteritems() }
HltSharedTrackLoc = { name : _baseTrackLocation(HltSharedTracksPrefix,loc) for name,loc in TrackName.iteritems() }

from HltTracking.Hlt1UpgradeTrackNames import Hlt1TrackLoc
HltDefaultFitSuffix 	                = "Fitted"  
#
# The recognised track types for the Hlt2 Tracking
#
Hlt2TrackingRecognizedTrackTypes = [ "Forward",
                                     "Long",
                                     "Downstream",
                                     "Best"]
#
# The recognised fit types for the Hlt2 Tracking
#
Hlt2TrackingRecognizedFitTypes = [ HltDefaultFitSuffix ]
#
Hlt2TrackingRecognizedFitTypesForRichID = [ HltDefaultFitSuffix ]
#

########################################################################
# ProtoParticles
########################################################################
# The suffix (this goes into the type field of _protosLocation)
#
Hlt2ChargedProtoParticleSuffix      = "Charged"
Hlt2NeutralProtoParticleSuffix      = "Neutrals"

#
########################################################################
# PID
########################################################################
# We want to generate PID containers for different track types the
# same way in which we generate tracks, meaning using the same rules,
# because we need different PID for different tracks; they should not
# overwrite each other!
#
HltSharedPIDPrefix = "PID"
HltNoPIDSuffix     = "NoPID"
HltAllPIDsSuffix   = "AllPIDs"
HltMuonIDSuffix    = "Muon"
HltRICHIDSuffix    = "RICH"
HltCALOIDSuffix    = "CALO"

# And the subdirectories. These are necessary so that different algorithms
# using e.g. different options for the RICH reco (radiators, hypotheses)
# don't clash with each other
#
HltAllPIDsProtosSuffix     = "WithAllPIDs"
HltCaloProtosSuffix        = "WithCaloID"
HltMuonProtosSuffix        = "WithMuonID"
HltCaloAndMuonProtosSuffix = "WithCaloAndMuonID"
HltRichProtosSuffix        = "WithRichID"

from HltTracking.Hlt1UpgradeTrackNames import Hlt1TracksPrefix, Hlt1TrackRoot
